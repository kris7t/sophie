/** @type {import('ts-jest').InitialOptionsTsJest} */
export default {
  projects: [
    '<rootDir>/packages/*',
  ],
  coverageProvider: 'v8',
  collectCoverageFrom: [
    'src/**/*.{ts,tsx}',
  ],
};
