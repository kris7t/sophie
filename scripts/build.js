import { build as esbuildBuild } from 'esbuild';
import { join } from 'path';
import { build as viteBuild } from 'vite';

import { fileURLToDirname } from '../config/utils.js';

const thisDir = fileURLToDirname(import.meta.url);

/**
 * @param {string} packageName
 * @returns {Promise<import('esbuild').BuildResult>}
 */
async function buildPackageEsbuild(packageName) {
  /** @type {{ default: import('esbuild').BuildOptions }} */
  const { default: config } = await import(`../packages/${packageName}/esbuild.config.js`);
  return esbuildBuild(config);
}

/**
 * @param {string} packageName
 * @returns {Promise<unknown>}
 */
function buildPackageVite(packageName) {
  return viteBuild({
    configFile: join(thisDir, `../packages/${packageName}/vite.config.js`),
  });
}

function buildAll() {
  const buildServiceShared = buildPackageEsbuild('service-shared');
  const buildShared = buildPackageEsbuild('shared');
  return Promise.all([
    Promise.all([
      buildServiceShared,
      buildShared,
    ]).then(() => buildPackageEsbuild('main')),
    buildServiceShared.then(() => Promise.all([
      buildPackageEsbuild('service-inject'),
      buildPackageEsbuild('service-preload'),
    ])),
    buildShared.then(() => Promise.all([
      buildPackageEsbuild('preload'),
      buildPackageVite('renderer'),
    ])),
  ]);
}

buildAll().catch((err) => {
  console.error(err);
  process.exit(1);
});
