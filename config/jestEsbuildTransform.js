import { transform } from 'esbuild';

import { node } from './buildConstants.js';

/** @type {import('@jest/transform').AsyncTransformer<undefined>} */
const transformer = {
  canInstrument: false,
  async processAsync(source, filePath) {
    const { code, map } = await transform(source, {
      loader: filePath.endsWith('tsx') ? 'tsx' : 'ts',
      sourcefile: filePath,
      format: 'esm',
      target: node,
      sourcemap: true,
    });
    return {
      code,
      map,
    };
  },
};

export default transformer;
