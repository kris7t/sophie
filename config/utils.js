import { dirname } from 'path';
import { fileURLToPath } from 'url';

/**
 * @param {string} url
 * @returns {string}
 */
export function fileURLToDirname(url) {
  return dirname(fileURLToPath(url));
}
