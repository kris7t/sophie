import { banner } from './buildConstants.js';

/** @type {string} */
const mode = process.env.MODE || 'development';

/** @type {boolean} */
const isDevelopment = mode === 'development';

/** @type {string} */
const modeString = JSON.stringify(mode);

/**
 * @param {import('esbuild').BuildOptions} config
 * @param {Record<string, unknown>} [extraMetaEnvVars]
 * @returns {import('esbuild').BuildOptions}
 */
export function getConfig(config, extraMetaEnvVars) {
  return {
    logLevel: 'info',
    bundle: true,
    treeShaking: !isDevelopment,
    minify: !isDevelopment,
    banner: {
      js: banner,
    },
    ...config,
    sourcemap: isDevelopment ? (config.sourcemap || true) : false,
    define: {
      'process.env.NODE_ENV': modeString,
      'process.env.MODE': modeString,
      'import.meta.env': JSON.stringify({
        DEV: isDevelopment,
        MODE: mode,
        PROD: !isDevelopment,
        ...extraMetaEnvVars,
      }),
    },
  };
}
