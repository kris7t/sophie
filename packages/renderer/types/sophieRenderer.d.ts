import type { SophieRenderer } from '@sophie/shared';

declare global {
  interface Window {
    readonly sophieRenderer: SophieRenderer;
  }
}
