/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  applySnapshot,
  applyPatch,
  Instance,
  types
} from 'mobx-state-tree';
import {
  BrowserViewBounds,
  sharedStore,
  SophieRenderer,
  ThemeSource,
} from '@sophie/shared';

import { getEnv, RendererEnv } from './RendererEnv.js';

export const rendererStore = types.model('RendererStore', {
  shared: types.optional(sharedStore, {}),
}).actions((self) => ({
  setBrowserViewBounds(browserViewBounds: BrowserViewBounds): void {
    getEnv(self).dispatchMainAction({
      action: 'set-browser-view-bounds',
      browserViewBounds,
    });
  },
  setThemeSource(themeSource: ThemeSource): void {
    getEnv(self).dispatchMainAction({
      action: 'set-theme-source',
      themeSource,
    });
  },
  toggleDarkMode(): void {
    if (self.shared.shouldUseDarkColors) {
      this.setThemeSource('light');
    } else {
      this.setThemeSource('dark');
    }
  },
}));

export interface RendererStore extends Instance<typeof rendererStore> {}

/**
 * Creates a new `RootStore` with a new environment and connects it to `ipc`.
 *
 * Changes to the `shared` store in the main process will be propagated to
 * the newly created store via `ipc`.
 *
 * @param ipc The `sophieRenderer` context bridge.
 */
export function createAndConnectRendererStore(ipc: SophieRenderer): RendererStore {
  const env: RendererEnv = {
    dispatchMainAction: ipc.dispatchAction,
  }
  const store = rendererStore.create({}, env);

  ipc.onSharedStoreChange({
    onSnapshot(snapshot) {
      applySnapshot(store.shared, snapshot);
    },
    onPatch(patch) {
      applyPatch(store.shared, patch);
    },
  }).catch((err) => {
    console.error('Failed to connect to shared store', err);
  });

  return store;
}
