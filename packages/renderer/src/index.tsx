/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import CssBaseline from "@mui/material/CssBaseline";
import React from 'react';
import { render } from 'react-dom';

import { App } from './components/App.jsx';
import { StoreProvider } from './components/StoreProvider.jsx';
import { ThemeProvider } from './components/ThemeProvider.jsx';
import { exposeToReduxDevtools, hotReloadServices } from './devTools.js';
import { createAndConnectRendererStore } from './stores/RendererStore.js';

const isDevelopment = import.meta.env.MODE === 'development';

if (isDevelopment) {
  hotReloadServices();
  document.title = `[dev] ${document.title}`;
}

const store = createAndConnectRendererStore(window.sophieRenderer);

if (isDevelopment) {
  exposeToReduxDevtools(store);
}

function Root(): JSX.Element {
  return (
    <React.StrictMode>
      <StoreProvider store={store}>
        <ThemeProvider>
          <CssBaseline enableColorScheme />
          <App />
        </ThemeProvider>
      </StoreProvider>
    </React.StrictMode>
  );
}

render(<Root />, document.querySelector('#app'));
