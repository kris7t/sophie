/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { throttle } from 'lodash';
import { observer } from 'mobx-react-lite';
import Box from '@mui/material/Box';
import React, { useCallback, useRef } from 'react';

import { useStore } from './StoreProvider.jsx';

export const BrowserViewPlaceholder = observer(function BrowserViewPlaceholder() {
  const {
    setBrowserViewBounds,
  } = useStore();

  const onResize = useCallback(throttle(([entry]: ResizeObserverEntry[]) => {
    if (entry) {
      const {
        x,
        y,
        width,
        height,
      } = entry.target.getBoundingClientRect();
      setBrowserViewBounds({
        x,
        y,
        width,
        height,
      });
    }
  }, 100), [setBrowserViewBounds]);

  const resizeObserverRef = useRef<ResizeObserver | null>(null);

  const ref = useCallback((element: HTMLElement | null) => {
    if (resizeObserverRef.current !== null) {
      resizeObserverRef.current.disconnect();
    }
    if (element === null) {
      resizeObserverRef.current = null;
      return;
    }
    resizeObserverRef.current = new ResizeObserver(onResize);
    resizeObserverRef.current.observe(element);
  }, [onResize, resizeObserverRef]);

  return (
    <Box
      sx={{
        flex: 1,
      }}
      ref={ref}
    />
  )
});
