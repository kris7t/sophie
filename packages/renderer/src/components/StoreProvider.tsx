/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import React, { createContext, useContext } from 'react';

import type { RendererStore } from '../stores/RendererStore.jsx';

const StoreContext = createContext<RendererStore | null>(null);

export function useStore(): RendererStore {
  const store = useContext(StoreContext);
  if (store === null) {
    throw new Error('useStore can only be called inside of StoreProvider');
  }
  return store;
}

export function StoreProvider({ children, store }: {
  children: JSX.Element | JSX.Element[],
  store: RendererStore,
}): JSX.Element {
  return (
    <StoreContext.Provider value={store}>
      {children}
    </StoreContext.Provider>
  );
}
