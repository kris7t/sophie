/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { observer } from 'mobx-react-lite';
import {
  unstable_createMuiStrictModeTheme as createTheme,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material/styles';
import React from 'react';

import { useStore } from './StoreProvider.jsx';

export const ThemeProvider = observer(function ThemeProvider({ children }: {
  children: JSX.Element | JSX.Element[],
}) {
  const { shared: { shouldUseDarkColors } } = useStore();

  const theme = createTheme({
    palette: {
      mode: shouldUseDarkColors ? 'dark' : 'light',
    },
  });

  return (
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  );
});
