/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { IAnyStateTreeNode } from 'mobx-state-tree';

/**
 * Connects the `model` to the redux devtools extension after loading the required
 * dependencies asynchronously.
 *
 * We have to apply a workaround to load `remotedev` by shimming the `global` object,
 * because `remotedev` uses an old version of `socketcluster-client` that refers to
 * `global` instead of `globalThis`.
 *
 * Due to the old dependencies, this function is not safe to call in production.
 * However, we don't bundle `remotedev` in production, so the call would fail anyways.
 *
 * @param model The store to connect to the redux devtools.
 * @see https://github.com/SocketCluster/socketcluster-client/issues/118#issuecomment-469064682
 */
async function exposeToReduxDevtoolsAsync(model: IAnyStateTreeNode): Promise<void> {
  (window as { global?: unknown }).global = window;

  const [remotedev, { connectReduxDevtools }] = await Promise.all([
    // @ts-ignore
    import('remotedev'),
    import('mst-middlewares'),
  ]);
  connectReduxDevtools(remotedev, model);
}

/**
 * Connects the `model` to the redux devtools extension.
 *
 * @param model The store to connect to the redux devtools.
 */
export function exposeToReduxDevtools(model: IAnyStateTreeNode): void {
  exposeToReduxDevtoolsAsync(model).catch((err) => {
    console.error('Could not connect to Redux devtools', err);
  });
}

/**
 * Sends a message to the main process to reload all services when
 * `build/watch.js` sends a reload event on bundle write.
 */
export function hotReloadServices(): void {
  import.meta.hot?.on('sophie:reload-services', () => {
    window.sophieRenderer.dispatchAction({
      action: 'reload-all-services',
    });
  });
}
