/* eslint-env node */

import { builtinModules } from 'module';
import { join } from 'path';
import react from '@vitejs/plugin-react';

import { banner, chrome } from '../../config/buildConstants.js';
import { fileURLToDirname } from '../../config/utils.js';

const thisDir = fileURLToDirname(import.meta.url);

const mode = process.env.MODE || 'development';

const isDevelopment = mode === 'development';

/**
 * @type {import('vite').UserConfig}
 * @see https://vitejs.dev/config/
 */
export default {
  /** @type {import('vite').LogLevel} */
  logLevel: 'info',
  mode,
  root: thisDir,
  cacheDir: join(thisDir, '../../.vite'),
  plugins: [
    react({
      babel: {
        // Gets rid of deoptimization warnings for large chunks.
        // We don't need to minify here, because the output of babel
        // will get passed to esbuild anyways.
        compact: false,
        minified: false,
      },
    }),
  ],
  base: '',
  server: {
    fs: {
      strict: true,
    },
  },
  resolve: {
    preserveSymlinks: true,
  },
  optimizeDeps: {
    link: [
      '@sophie/shared',
    ],
  },
  build: {
    target: chrome,
    assetsDir: '.',
    outDir: 'dist',
    emptyOutDir: true,
    sourcemap: isDevelopment,
    minify: !isDevelopment,
    brotliSize: false,
    rollupOptions: {
      external: [
        'mst-middlewares',
        'remotedev',
        ...builtinModules,
      ],
      output: {
        banner,
      }
    },
  },
};
