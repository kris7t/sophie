import { chrome } from '../../config/buildConstants.js';
import { getConfig } from '../../config/esbuildConfig.js';
import { fileURLToDirname } from '../../config/utils.js';

export default getConfig({
  absWorkingDir: fileURLToDirname(import.meta.url),
  entryPoints: [
    'src/index.ts',
  ],
  outfile: 'dist/index.cjs',
  format: 'cjs',
  platform: 'node',
  target: chrome,
  sourcemap: 'inline',
  external: [
    'electron',
  ],
});
