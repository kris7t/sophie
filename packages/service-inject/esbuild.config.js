import { chrome } from '../../config/buildConstants.js';
import { getConfig } from '../../config/esbuildConfig.js';
import { fileURLToDirname } from '../../config/utils.js';

export default getConfig({
  absWorkingDir: fileURLToDirname(import.meta.url),
  entryPoints: [
    'src/index.ts',
  ],
  outfile: 'dist/index.js',
  format: 'iife',
  platform: 'browser',
  target: chrome,
  sourcemap: 'inline',
});
