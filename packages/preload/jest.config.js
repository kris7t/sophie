import rootConfig from '../../config/jest.config.base.js';

/** @type {import('ts-jest').InitialOptionsTsJest} */
export default {
  ...rootConfig,
  testEnvironment: 'jsdom',
};
