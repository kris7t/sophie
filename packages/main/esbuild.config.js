import { node } from '../../config/buildConstants.js';
import { getConfig } from '../../config/esbuildConfig.js';
import { fileURLToDirname } from '../../config/utils.js';

const externalPackages = ['electron'];

if (process.env.MODE !== 'development') {
  externalPackages.push('electron-devtools-installer');
}

export default getConfig({
  absWorkingDir: fileURLToDirname(import.meta.url),
  entryPoints: [
    'src/index.ts',
  ],
  outfile: 'dist/index.cjs',
  format: 'cjs',
  platform: 'node',
  target: node,
  external: externalPackages,
}, {
  VITE_DEV_SERVER_URL: process.env.VITE_DEV_SERVER_URL || null,
});
