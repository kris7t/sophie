interface ImportMeta {
  env: {
    DEV: boolean;
    MODE: string;
    PROD: boolean;
    VITE_DEV_SERVER_URL: string;
  }
}
