/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import chalk, { ChalkInstance } from 'chalk';
import loglevel, { Logger } from 'loglevel';
import prefix from 'loglevel-plugin-prefix';

if (import.meta.env?.DEV) {
  loglevel.setLevel('debug');
} else {
  loglevel.setLevel('info');
}

const COLORS: Partial<Record<string, ChalkInstance>> = {
  TRACE: chalk.magenta,
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red,
  CRITICAL: chalk.red,
};

function getColor(level: string): ChalkInstance {
  return COLORS[level] ?? chalk.gray;
}

prefix.reg(loglevel);
prefix.apply(loglevel, {
  format(level, name, timestamp) {
    const levelColor = getColor(level);
    return `${chalk.gray(`[${timestamp}]`)} ${levelColor(level)} ${chalk.green(`${name}:`)}`;
  },
});

export function getLogger(loggerName: string): Logger {
  return loglevel.getLogger(loggerName);
}

export function silenceLogger(): void {
  loglevel.disableAll();
  const loggers = loglevel.getLoggers();
  for (const loggerName of Object.keys(loggers)) {
    loggers[loggerName].disableAll();
  }
}
