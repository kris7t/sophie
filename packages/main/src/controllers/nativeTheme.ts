/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { nativeTheme } from 'electron';
import { autorun } from 'mobx';

import type { MainStore } from '../stores/MainStore.js';
import { Disposer } from '../utils/disposer.js';
import { getLogger } from '../utils/logging.js';

const log = getLogger('nativeTheme');

export function initNativeTheme(store: MainStore): Disposer {
  log.trace('Initializing nativeTheme controller');

  const disposeThemeSourceReaction = autorun(() => {
    nativeTheme.themeSource = store.config.themeSource;
    log.debug('Set theme source:', store.config.themeSource);
  });

  store.setShouldUseDarkColors(nativeTheme.shouldUseDarkColors);
  const shouldUseDarkColorsListener = () => {
    store.setShouldUseDarkColors(nativeTheme.shouldUseDarkColors);
    log.debug('Set should use dark colors:', nativeTheme.shouldUseDarkColors);
  };
  nativeTheme.on('updated', shouldUseDarkColorsListener);

  return () => {
    log.trace('Disposing nativeTheme controller');
    nativeTheme.off('updated', shouldUseDarkColorsListener);
    disposeThemeSourceReaction();
  };
}
