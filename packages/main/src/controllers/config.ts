/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { debounce } from 'lodash-es';
import ms from 'ms';
import { applySnapshot, getSnapshot, onSnapshot } from 'mobx-state-tree';

import type { ConfigPersistenceService } from '../services/ConfigPersistenceService.js';
import type { Config, ConfigSnapshotOut } from '../stores/Config.js';
import { Disposer } from '../utils/disposer.js';
import { getLogger } from '../utils/logging.js';

const DEFAULT_CONFIG_DEBOUNCE_TIME = ms('1s');

const log = getLogger('config');

export async function initConfig(
  config: Config,
  persistenceService: ConfigPersistenceService,
  debounceTime: number = DEFAULT_CONFIG_DEBOUNCE_TIME,
): Promise<Disposer> {
  log.trace('Initializing config controller');

  let lastSnapshotOnDisk: ConfigSnapshotOut | null = null;

  async function readConfig(): Promise<boolean> {
    const result = await persistenceService.readConfig();
    if (result.found) {
      try {
        applySnapshot(config, result.data);
        lastSnapshotOnDisk = getSnapshot(config);
      } catch (err) {
        log.error('Failed to apply config snapshot', result.data, err);
      }
    }
    return result.found;
  }

  async function writeConfig(): Promise<void> {
    const snapshot = getSnapshot(config);
    await persistenceService.writeConfig(snapshot);
    lastSnapshotOnDisk = snapshot;
  }

  if (!await readConfig()) {
    log.info('Config file was not found');
    await writeConfig();
    log.info('Created config file');
  }

  const disposeOnSnapshot = onSnapshot(config, debounce((snapshot) => {
    // We can compare snapshots by reference, since it is only recreated on store changes.
    if (lastSnapshotOnDisk !== snapshot) {
      writeConfig().catch((err) => {
        log.error('Failed to write config on config change', err);
      });
    }
  }, debounceTime));

  const disposeWatcher = persistenceService.watchConfig(async () => {
    try {
      await readConfig();
    } catch (err) {
      log.error('Failed to read config', err);
    }
  }, debounceTime);

  return () => {
    log.trace('Disposing config controller');
    disposeWatcher();
    disposeOnSnapshot();
  };
}
