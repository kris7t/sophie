/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { jest } from '@jest/globals';
import { mocked } from 'jest-mock';

import { createMainStore, MainStore } from '../../stores/MainStore.js';
import { Disposer } from '../../utils/disposer.js';

let shouldUseDarkColors = false;

jest.unstable_mockModule('electron', () => ({
  nativeTheme: {
    themeSource: 'system',
    get shouldUseDarkColors() {
      return shouldUseDarkColors;
    },
    on: jest.fn(),
    off: jest.fn(),
  },
}));

const { nativeTheme } = await import('electron');
const { initNativeTheme } = await import('../nativeTheme');

let store: MainStore;
let disposeSut: Disposer;

beforeEach(() => {
  store = createMainStore();
  disposeSut = initNativeTheme(store);
});

it('should register a nativeTheme updated listener', () => {
  expect(nativeTheme.on).toBeCalledWith('updated', expect.anything());
});

it('should synchronize themeSource changes to the nativeTheme', () => {
  store.config.setThemeSource('dark');
  expect(nativeTheme.themeSource).toBe('dark');
});

it('should synchronize shouldUseDarkColors changes to the store', () => {
  const listener = mocked(nativeTheme.on).mock.calls.find(([event]) => event === 'updated')![1];
  shouldUseDarkColors = true;
  listener();
  expect(store.shared.shouldUseDarkColors).toBe(true);
});

it('should remove the listener on dispose', () => {
  const listener = mocked(nativeTheme.on).mock.calls.find(([event]) => event === 'updated')![1];
  disposeSut();
  expect(nativeTheme.off).toBeCalledWith('updated', listener);
});
