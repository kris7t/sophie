/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { jest } from '@jest/globals';
import { mocked } from 'jest-mock';
import ms from 'ms';

import { initConfig } from '../config';
import type { ConfigPersistenceService } from '../../services/ConfigPersistenceService.js';
import { Config, config as configModel } from '../../stores/Config.js';
import { Disposer } from '../../utils/disposer.js';
import { silenceLogger } from '../../utils/logging.js';

let config: Config;
let persistenceService: ConfigPersistenceService = {
  readConfig: jest.fn(),
  writeConfig: jest.fn(),
  watchConfig: jest.fn(),
};
let lessThanThrottleMs = ms('0.1s');
let throttleMs = ms('1s');

beforeAll(() => {
  jest.useFakeTimers();
  silenceLogger();
});

beforeEach(() => {
  config = configModel.create();
});

describe('when initializing', () => {
  describe('when there is no config file', () => {
    beforeEach(() => {
      mocked(persistenceService.readConfig).mockResolvedValueOnce({
        found: false,
      });
    });

    it('should create a new config file', async () => {
      await initConfig(config, persistenceService);
      expect(persistenceService.writeConfig).toBeCalledTimes(1);
    });

    it('should bail if there is an an error creating the config file', async () => {
      mocked(persistenceService.writeConfig).mockRejectedValue(new Error('boo'));
      await expect(() => initConfig(config, persistenceService)).rejects.toBeInstanceOf(Error);
    });
  });

  describe('when there is a valid config file', () => {
    beforeEach(() => {
      mocked(persistenceService.readConfig).mockResolvedValueOnce({
        found: true,
        data: {
          themeSource: 'dark',
        },
      });
    });

    it('should read the existing config file is there is one', async () => {
      await initConfig(config, persistenceService);
      expect(persistenceService.writeConfig).not.toBeCalled();
      expect(config.themeSource).toBe('dark');
    });

    it('should bail if it cannot set up a watcher', async () => {
      mocked(persistenceService.watchConfig).mockImplementationOnce(() => {
        throw new Error('boo');
      });
      await expect(() => initConfig(config, persistenceService)).rejects.toBeInstanceOf(Error);
    });
  });

  it('should not apply an invalid config file', async () => {
    mocked(persistenceService.readConfig).mockResolvedValueOnce({
      found: true,
      data: {
        themeSource: -1,
      },
    });
    await initConfig(config, persistenceService);
    expect(config.themeSource).not.toBe(-1);
  });

  it('should bail if it cannot determine whether there is a config file', async () => {
    mocked(persistenceService.readConfig).mockRejectedValue(new Error('boo'));
    await expect(() => initConfig(config, persistenceService)).rejects.toBeInstanceOf(Error);
  });
});

describe('when it has loaded the config', () => {
  let sutDisposer: Disposer;
  let watcherDisposer: Disposer = jest.fn();
  let configChangedCallback: () => Promise<void>;

  beforeEach(async () => {
    mocked(persistenceService.readConfig).mockResolvedValueOnce({
      found: true,
      data: {},
    });
    mocked(persistenceService.watchConfig).mockReturnValueOnce(watcherDisposer);
    sutDisposer = await initConfig(config, persistenceService, throttleMs);
    configChangedCallback = mocked(persistenceService.watchConfig).mock.calls[0][0];
    jest.resetAllMocks();
  });

  it('should throttle saving changes to the config file', () => {
    mocked(persistenceService.writeConfig).mockResolvedValue(undefined);
    config.setThemeSource('dark');
    jest.advanceTimersByTime(lessThanThrottleMs);
    config.setThemeSource('light');
    jest.advanceTimersByTime(throttleMs);
    expect(persistenceService.writeConfig).toBeCalledTimes(1);
  });

  it('should handle config writing errors gracefully', () => {
    mocked(persistenceService.writeConfig).mockRejectedValue(new Error('boo'));
    config.setThemeSource('dark');
    jest.advanceTimersByTime(throttleMs);
    expect(persistenceService.writeConfig).toBeCalledTimes(1);
  });

  it('should read the config file when it has changed', async () => {
    mocked(persistenceService.readConfig).mockResolvedValueOnce({
      found: true,
      data: {
        themeSource: 'dark',
      },
    });
    await configChangedCallback();
    // Do not write back the changes we have just read.
    expect(persistenceService.writeConfig).not.toBeCalled();
    expect(config.themeSource).toBe('dark');
  });

  it('should not apply an invalid config file when it has changed', async () => {
    mocked(persistenceService.readConfig).mockResolvedValueOnce({
      found: true,
      data: {
        themeSource: -1,
      },
    });
    await configChangedCallback();
    expect(config.themeSource).not.toBe(-1);
  });

  it('should handle config writing errors gracefully', async () => {
    mocked(persistenceService.readConfig).mockRejectedValue(new Error('boo'));
    await configChangedCallback();
  });

  describe('when it was disposed', () => {
    beforeEach(() => {
      sutDisposer();
    });

    it('should dispose the watcher', () => {
      expect(watcherDisposer).toBeCalled();
    });

    it('should not listen to store changes any more', () => {
      config.setThemeSource('dark');
      jest.advanceTimersByTime(2 * throttleMs);
      expect(persistenceService.writeConfig).not.toBeCalled();
    });
  });
});
