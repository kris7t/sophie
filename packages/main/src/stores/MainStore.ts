/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { applySnapshot, Instance, types } from 'mobx-state-tree';
import { BrowserViewBounds } from '@sophie/shared';

import type { Config } from './Config.js';
import { sharedStore } from './SharedStore.js';

export const mainStore = types.model('MainStore', {
  browserViewBounds: types.optional(types.model('BrowserViewBounds', {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  }), {}),
  shared: types.optional(sharedStore, {}),
}).views((self) => ({
  get config(): Config {
    return self.shared.config;
  },
})).actions((self) => ({
  setBrowserViewBounds(bounds: BrowserViewBounds): void {
    applySnapshot(self.browserViewBounds, bounds);
  },
  setShouldUseDarkColors(shouldUseDarkColors: boolean): void {
    self.shared.shouldUseDarkColors = shouldUseDarkColors;
  }
}));

export interface MainStore extends Instance<typeof mainStore> {}

export function createMainStore(): MainStore {
  return mainStore.create();
}
