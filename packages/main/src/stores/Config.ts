/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { Instance } from 'mobx-state-tree';
import {
  config as originalConfig,
  ConfigSnapshotIn,
  ConfigSnapshotOut,
  ThemeSource,
} from '@sophie/shared';

export const config = originalConfig.actions((self) => ({
  setThemeSource(mode: ThemeSource) {
    self.themeSource = mode;
  },
}));

export interface Config extends Instance<typeof config> {}

export type { ConfigSnapshotIn, ConfigSnapshotOut };
