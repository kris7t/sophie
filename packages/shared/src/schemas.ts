/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { z } from 'zod';

export const browserViewBounds = z.object({
  x: z.number().nonnegative(),
  y: z.number().nonnegative(),
  width: z.number().nonnegative(),
  height: z.number().nonnegative(),
});

export type BrowserViewBounds = z.infer<typeof browserViewBounds>;

const setBrowserViewBoundsAction = z.object({
  action: z.literal('set-browser-view-bounds'),
  browserViewBounds,
});

export const themeSource = z.enum(['system', 'light', 'dark']);

export type ThemeSource = z.infer<typeof themeSource>;

const setThemeSourceAction = z.object({
  action: z.literal('set-theme-source'),
  themeSource,
});

const reloadAllServicesAction = z.object({
  action: z.literal('reload-all-services'),
});

export const action = z.union([
  setBrowserViewBoundsAction,
  setThemeSourceAction,
  reloadAllServicesAction,
]);

export type Action = z.infer<typeof action>;
