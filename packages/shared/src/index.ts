/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

export type { SophieRenderer } from './contextBridge/SophieRenderer';

export {
  MainToRendererIpcMessage,
  RendererToMainIpcMessage,
} from './ipc.js';

export type {
  Action,
  BrowserViewBounds,
  ThemeSource,
} from './schemas.js';
export {
  action,
  browserViewBounds,
  themeSource,
} from './schemas.js';

export type { Config, ConfigSnapshotIn, ConfigSnapshotOut } from './stores/Config.js';
export { config } from './stores/Config.js';

export type {
  SharedStore,
  SharedStoreListener,
  SharedStoreSnapshotIn,
  SharedStoreSnapshotOut,
} from './stores/SharedStore.js';
export { sharedStore } from './stores/SharedStore.js';
