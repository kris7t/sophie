import { chrome, node } from '../../config/buildConstants.js';
import { getConfig } from '../../config/esbuildConfig.js';
import { fileURLToDirname } from '../../config/utils.js';

export default getConfig({
  absWorkingDir: fileURLToDirname(import.meta.url),
  entryPoints: [
    'src/index.ts',
  ],
  outfile: 'dist/index.mjs',
  format: 'esm',
  // The package that includes this one will have a header comment,
  // no need to have an additional one here.
  banner: {},
  platform: 'node',
  target: [chrome, node],
  external: [
    'mobx',
    'mobx-state-tree',
    'zod',
  ],
});
