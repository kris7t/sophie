// @ts-check

const { Arch } = require('electron-builder');
const { flipFuses, FuseV1Options, FuseVersion } = require('@electron/fuses');
const { join } = require('path');

/**
 * @type {import('electron-builder').Configuration}
 * @see https://www.electron.build/configuration/configuration
 */
const config = {
  directories: {
    output: 'dist',
    buildResources: 'buildResources',
  },
  files: [
    'packages/main/dist/**',
    'packages/preload/dist/**',
    'packages/renderer/dist/**',
    'packages/service-inject/dist/**',
    'packages/service-preload/dist/**',
    // Do not ship with source maps.
    '!**/*.map',
  ],
  afterPack(context) {
    return burnFuses(context);
  },
};

/**
 * Hardens the shipped electron binary by burning some electron fuses.
 *
 * Enabled chromium cookie encryption and disables options that could be
 * used to execute arbitrary code in the main process to circumvent cookie encryption:
 * - Running the application as a plain node process is disabled.
 * - Setting options through the `NODE_OPTIONS` environment variable is disabled.
 * - Attaching a debugger through the `--inspect` family of options is disabled.
 * - Will onload load the application from the ASAR archive.
 *
 * @param {import('electron-builder').AfterPackContext} context The `electron-builder` context.
 * @return {Promise<void>} The promise to flip the fuses.
 * @see https://github.com/electron/fuses
 */
async function burnFuses(context) {
  /** @type {string} */
  const ext = {
    darwin: '.app',
    win32: '.exe',
  }[context.electronPlatformName] || '';
  const electronBinaryPath = join(
    context.appOutDir,
    `${context.packager.appInfo.productFilename}${ext}`
  );
  /** @type {import('@electron/fuses').FuseConfig<boolean>} */
  const fuseConfig = {
    version: FuseVersion.V1,
    resetAdHocDarwinSignature: context.electronPlatformName === 'darwin' && context.arch === Arch.arm64,
    [FuseV1Options.RunAsNode]: false,
    [FuseV1Options.EnableCookieEncryption]: true,
    [FuseV1Options.EnableNodeOptionsEnvironmentVariable]: false,
    [FuseV1Options.EnableNodeCliInspectArguments]: false,
    // TODO: Revisit this: IF set to 'true' the packaged app doesn't start up on macos (x86)
    [FuseV1Options.EnableEmbeddedAsarIntegrityValidation]: false,
    [FuseV1Options.OnlyLoadAppFromAsar]: true,
  };
  return flipFuses(electronBinaryPath, fuseConfig);
}

module.exports = config;
